#include<iostream>
#include<stdlib.h>
#include<conio.h>

typedef unsigned short int uint;
void Coding(const std::string ,std::string&);
void DeCode(const std::string ,std::string&);
int main(){
    while(true){
        std::string _S,_D;
        std::cout<<"Enter Your String :-->";
        std::getline(std::cin,_S);

        system("cls");
        Coding(_S,_D);
        std::cout<<"Your String is--:"<<_S<<"\nCodede is: "<<_D<<'\n';
        DeCode(_D,_S);
        std::cout<<"DeCode is: "<<_S<<'\n';
        getch();
        system("cls");
    }

    return 0;
}
uint num_[]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,
            15,16,17,18,19,20,21,22,23,24,25,
            26,27,28,29,30,31,32,33,34,35,36,
            37,38,39,40,41,42,43,44,45,46,47,
            48,49,50,51,52};
char chr_[]{'A','B','C','D','E','F','G','H',
            'I','J','K','L','M','N','O','P',
            'Q','R','S','T','U','V','W','X',
            'Y','Z',
            'a','b','c','d','e','f','g','h',
            'i','j','k','l','m','n','o','p',
            'q','r','s','t','u','v','w','x',
            'y','z'};
uint num_ret(char chPtr){
    for(int i=1;i<=52;i++){
        if(chPtr==chr_[i])
            return num_[i]+50;
    }
}
uint chr_ret(uint inPtr){
    for(int i=1;i<=52;i++){
        if((inPtr-50) == num_[i])
            return (chr_[i]);
    }
}
void Coding(const std::string _sorce,std::string &_destination){
    std::string result;
    for(int i=0;i<_sorce.length();i++){
        result+=(char)num_ret(_sorce[i]);
    }
    _destination=result;
}

void DeCode(const std::string _sorce,std::string &_destination){
    std::string result;
    for(int i=0;i<_sorce.length();i++){
        result+=(char)chr_ret(_sorce[i]);
    }
    _destination=result;
}
